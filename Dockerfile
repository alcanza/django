FROM python:3.10.8-slim

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY ./requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt
COPY ./ /usr/src/app

EXPOSE 8000
COPY entrypoint.sh /usr/src/app/entrypoint.sh
RUN chmod +x /usr/src/app/entrypoint.sh

# Instala gettext
RUN apt-get update && apt-get install -y gettext && rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/usr/src/app/entrypoint.sh"]
