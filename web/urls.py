from django.urls import path
from django.conf.urls import include

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("residencia-canina", views.residencia_canina,
         name="residencia_canina"),
    path("educacion-canina", views.educacion_canina, name="educacion_canina"),
    path("intervenciones-asistidas-por-perros",
         views.intervenciones_asistidas_por_perros,
         name="intervenciones_asistidas_por_perros"),
    path("nuestro-club", views.nuestro_club, name="nuestro_club"),
    path("equipo", views.equipo, name="equipo"),
    path("contacto", views.contacto, name="contacto"),
    path("instalaciones", views.instalaciones, name="instalaciones"),

    # Cambio idoma
    path('language/', views.change_language, name='change_language'),
]
