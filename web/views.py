from django.shortcuts import render, redirect
from django.utils import translation
from django.conf import settings
import logging

logger = logging.getLogger(__name__)


def index(request):
    return render(request, 'index.html')


def residencia_canina(request):
    return render(request, 'residencia_canina.html')


def educacion_canina(request):
    return render(request, 'educacion_canina.html')


def intervenciones_asistidas_por_perros(request):
    return render(request, 'intervenciones_asistidas_por_perros.html')

def nuestro_club(request):
    return render(request, 'nuestro_club.html')


def equipo(request):
    return render(request, 'equipo.html')


def contacto(request):
    return render(request, 'contacto.html')


def instalaciones(request):
    return render(request, 'instalaciones.html')


def google(request):
    return render(request, 'google.html')


def change_language(request):
    if settings.LANGUAGE_COOKIE_NAME not in request.session:
        request.session[settings.LANGUAGE_COOKIE_NAME] = "es"
    current_language = request.session[settings.LANGUAGE_COOKIE_NAME]
    
    if current_language == "gl":
        lang_code = "es"
    else:
        lang_code = "gl"

    translation.activate(lang_code)
    request.session[settings.LANGUAGE_COOKIE_NAME] = lang_code

    next_url = request.META.get('HTTP_REFERER', '/')
    response = redirect(next_url)
    return response