from django.utils import translation
from django.utils.deprecation import MiddlewareMixin
from django.conf import settings


class SessionLocaleMiddleware(MiddlewareMixin):
    def process_request(self, request):
        language = request.session.get(settings.LANGUAGE_COOKIE_NAME)
        if language:
            translation.activate(language)
        else:
            language = translation.get_language_from_request(request)
            translation.activate(language)
            request.session[settings.LANGUAGE_COOKIE_NAME] = language
        request.LANGUAGE_CODE = translation.get_language()

    def process_response(self, request, response):
        language = translation.get_language()
        if language and settings.LANGUAGE_COOKIE_NAME in request.session:
            request.session[settings.LANGUAGE_COOKIE_NAME] = language
        return response

