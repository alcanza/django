#!/bin/bash

# Ejecutar los comandos de Django
python manage.py collectstatic --noinput
python manage.py makemigrations --noinput
python manage.py migrate --noinput
python manage.py compilemessages --noinput
gunicorn alcanza.wsgi --bind=0.0.0.0:8000 --workers 3 --log-level=debug
