from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.urls import reverse
from django.utils.text import slugify
from taggit.managers import TaggableManager
from django.utils.safestring import mark_safe
import markdown

class Post(models.Model):
    STATUS_CHOICES = (
        ('draft', 'Draft'),
        ('published', 'Published'),
    )

    title = models.CharField(max_length=200, verbose_name="Título")
    slug = models.SlugField(
        max_length=200,
        unique=True,
        verbose_name="URL amigable"
    )
    author = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='blog_posts',
        verbose_name="Autor"
    )
    summary = models.TextField(
        max_length=500,
        verbose_name="Resumen",
        help_text="Breve resumen del post (máximo 500 caracteres)"
    )
    content = models.TextField(verbose_name="Contenido")
    featured_image = models.FileField(
        upload_to='blog/',
        blank=True,
        null=True,
        verbose_name="Imagen destacada"
    )
    tags = TaggableManager(verbose_name="Etiquetas", blank=True)
    
    # SEO Fields
    meta_description = models.CharField(
        max_length=160,
        blank=True,
        verbose_name="Meta descripción",
        help_text="Descripción para motores de búsqueda (máximo 160 caracteres)"
    )
    meta_keywords = models.CharField(
        max_length=255,
        blank=True,
        verbose_name="Meta palabras clave",
        help_text="Palabras clave separadas por comas"
    )
    
    publish = models.DateTimeField(default=timezone.now, verbose_name="Fecha de publicación")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de actualización")
    status = models.CharField(
        max_length=10,
        choices=STATUS_CHOICES,
        default='draft',
        verbose_name="Estado"
    )

    class Meta:
        ordering = ('-publish',)
        verbose_name = "Post"
        verbose_name_plural = "Posts"

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('blog:post_detail', args=[self.slug])
    
    def get_markdown_content(self):
        # Convertir Markdown a HTML con extensiones comunes
        return mark_safe(markdown.markdown(
            self.content,
            extensions=[
                'markdown.extensions.fenced_code',
                'markdown.extensions.tables',
                'markdown.extensions.codehilite',
                'markdown.extensions.toc',
                'markdown.extensions.nl2br'
            ]
        ))