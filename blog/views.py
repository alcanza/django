# blog/views.py
from django.views.generic import ListView, DetailView
from django.shortcuts import get_object_or_404
from .models import Post
from taggit.models import Tag
from django.db.models import Count

class PostListView(ListView):
    model = Post
    template_name = 'blog/list.html'
    context_object_name = 'posts'
    paginate_by = 6

    def get_queryset(self):
        queryset = Post.objects.filter(status='published')
        
        # Filtrar por tag si existe
        tag_slug = self.kwargs.get('tag_slug')
        if tag_slug:
            tag = get_object_or_404(Tag, slug=tag_slug)
            queryset = queryset.filter(tags__in=[tag])
            
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        # Agregar tag al contexto si existe
        tag_slug = self.kwargs.get('tag_slug')
        if tag_slug:
            context['tag'] = get_object_or_404(Tag, slug=tag_slug)
            
        # Agregar tags populares
        context['popular_tags'] = Tag.objects.annotate(
            posts_count=Count('taggit_taggeditem_items')
        ).order_by('-posts_count')[:10]
        context['all_tags'] = Tag.objects.annotate(
            posts_count=Count('taggit_taggeditem_items')
        ).order_by('name')
        
        return context

class PostDetailView(DetailView):
    model = Post
    template_name = 'blog/detail.html'
    context_object_name = 'post'
    
    def get_queryset(self):
        return Post.objects.filter(status='published')
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        # Obtener post actual
        post = self.get_object()
        
        # Posts relacionados por tags
        post_tags_ids = post.tags.values_list('id', flat=True)
        similar_posts = Post.objects.filter(status='published')\
                                  .filter(tags__in=post_tags_ids)\
                                  .exclude(id=post.id)
        similar_posts = similar_posts.annotate(same_tags=Count('tags'))\
                                   .order_by('-same_tags', '-publish')[:3]
        context['all_tags'] = Tag.objects.annotate(
            posts_count=Count('taggit_taggeditem_items')
        ).order_by('name')
        
        context['similar_posts'] = similar_posts
        return context