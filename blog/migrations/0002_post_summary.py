# Generated by Django 5.0.4 on 2025-01-27 14:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='summary',
            field=models.TextField(default='', help_text='Breve resumen del post (máximo 500 caracteres)', max_length=500, verbose_name='Resumen'),
            preserve_default=False,
        ),
    ]
