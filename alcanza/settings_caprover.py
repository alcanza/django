import os
from datetime import timedelta
from typing import List, Tuple
from .settings import INSTALLED_APPS

# key and debugging settings should not changed without care
SECRET_KEY = os.environ.get("CR_SECRET_KEY")
DEBUG = False

# allowed hosts get parsed from a comma-separated list
hosts = os.environ.get("CR_HOSTS")

ALLOWED_HOSTS = hosts.split(",")


SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SECURE_SSL_REDIRECT = True

# Database

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.environ.get("CR_DB_NAME"),
        "USER": os.environ.get("CR_DB_USER"),
        "PASSWORD": os.environ.get("CR_DB_PASSWORD"),
        "HOST": os.environ.get("CR_DB_HOST"),
        "PORT": os.environ.get("CR_DB_PORT"),
    }
}

INSTALLED_APPS.append('django_minio_backend')

# Minio Storage

MINIO_ENDPOINT = os.environ.get("MINIO_ENDPOINT")
MINIO_EXTERNAL_ENDPOINT = os.environ.get("MINIO_ENDPOINT")
MINIO_EXTERNAL_ENDPOINT_USE_HTTPS = True 
MINIO_ACCESS_KEY = os.environ.get("MINIO_ACCESS_KEY")
MINIO_SECRET_KEY = os.environ.get("MINIO_SECRET_KEY")
MINIO_USE_HTTPS = True
MINIO_URL_EXPIRY_HOURS = timedelta(days=7)  # Default is 7 days (longest) if not defined
MINIO_CONSISTENCY_CHECK_ON_START = True
MINIO_PRIVATE_BUCKETS = [
    'alcanza-private',
]
MINIO_PUBLIC_BUCKETS = [
    'alcanza-public',
]
MINIO_POLICY_HOOKS: List[Tuple[str, dict]] = []
MINIO_MEDIA_FILES_BUCKET = 'alcanza-media'  # replacement for MEDIA_ROOT
MINIO_STATIC_FILES_BUCKET = 'alcanza-static'  # replacement for STATIC_ROOT
MINIO_BUCKET_CHECK_ON_SAVE = True 

STATICFILES_STORAGE = 'django_minio_backend.models.MinioBackendStatic'
MINIO_STATIC_FILES_BUCKET = 'alcanza-static' 

STATIC_URL = f'http://{MINIO_EXTERNAL_ENDPOINT}/{MINIO_STATIC_FILES_BUCKET}/'
MEDIA_URL = f'http://{MINIO_EXTERNAL_ENDPOINT}/{MINIO_MEDIA_FILES_BUCKET}/'

DEFAULT_FILE_STORAGE = 'django_minio_backend.models.MinioBackend'

MINIO_PUBLIC_BUCKETS.append(MINIO_STATIC_FILES_BUCKET)
MINIO_PUBLIC_BUCKETS.append(MINIO_MEDIA_FILES_BUCKET)
